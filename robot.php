<?php 
/**
 * This robot class controls all functions that is required to control the robot on a tabletop
 * Php Version 7.1
 * Known limitations:
 * 		If the coords are seperated into two seperate locations within the command block the code will throw back an error on the X or Y coords based on which one was seperated.
 * Usage:
 * Intialise the tabletop object first then pass it as a parameter to the new robot object
 * It will auto load the command file and run straight away
 * 
 * @depends new tabletop() to be initialization in order to use, otherwise the robot fall onto the floor and break
 * @author Steven Field <steven@defineware.com>
 * @version 1.0
 * @datetime 7/10/20
 */
class robot{
	const COMPASS = array(0=>"NORTH",1=>"EAST",2=>"SOUTH",3=>"WEST");
	CONST TURN = array(0=>"UP", 1=>"RIGHT", 2=>"DOWN", 3=>"LEFT");
	const KEYWORD = 'PLACE';
	const MOVE_KEYWORD = 'MOVE';
	const REPORT_KEYWORD = 'REPORT';

	var $x = 0, $y = 0;//default values
	var $direction = 0;//default values
	var $table = array();//acts as an referance to the table for the robot to navigate in the event there is another robot that can't share the same location
	

	/**
	 * Construct the robot using the tabletop as referance point
	 *
	 * @param [Object] $tabletop - Referance object of the tabletop
	 */
	function __construct(&$tabletop){
		$this->table = $tabletop;
		
	}
	
	/**
	 * Move the robot to the coords given or for it's inital place on the tabletop
	 *
	 * @param [int] $x - x axis
	 * @param [int] $y - y axis
	 * @return void
	 */
	function jump($x, $y){
		//validate the coords are positive
		if($x < 0 || $y < 0){
			return "<br/>Out of bounds!<br/>";
		}
		//check if the column exists on the table
		if(array_key_exists($y,$this->table->table_top )){
			if(array_key_exists($x,$this->table->table_top[$y])){
				$this->x = $x;//set these so they are accessable later on it's current location
				$this->y = $y;
				return;
			}else{
				return "<br/>Out of bounds on X<br/>";
			}
		}else{
			return "<br/>Out of bounds on Y<br/>";
		}
	}

	/**
	 * Look up the current table for the location of the robot
	 *
	 * @return void - string of location data from object  
	 */
	function get_postion(){
		return "{$this->x}, {$this->y}";	
	}

	/**
	 * Search the raw data if the keyword is found, enforce uppercase to protect against mixed case text
	 *
	 * @param [string] $cmd - raw command data
	 * @return void
	 */
	function search_keyword($cmd){
		$words = explode(' ',$cmd);
		if(in_array(strtoupper(self::KEYWORD),$words)){
			$postion = array_search(strtoupper(self::KEYWORD),$words);
			return $postion;
		}
	}

	
	/**
	 * Finds and strips off the keyword of the raw string and explodes the rest by the comma delimiter
	 *
	 * @param [int] $key_pos - The position of the keyword to remove the non-needed data from
	 * @param [string] $cmd - raw command data
	 * @return void
	 */
	function get_commands($key_pos,$cmd){
		$command = substr($cmd,strlen(self::KEYWORD)+$key_pos+1);//remove any text before the keyword and remove up to the extra space
		$command = $this->clean_string($command);
		
		return explode(',',$command);
	}

	/**
	 * Print out on screen the current coords of the robot and what was the last direction command
	 *
	 * @return void
	 */
	function report(){
		$postion = $this->get_postion();
		$direction = $this->get_direction();
		echo "<br/><strong>{$postion}, {$direction}</strong></br>";
	}

	/**
	 * Takes the raw data changes it to something more useful
	 * This is were the magic happens
	 *
	 * @param [string] $cmd - raw string block with keyword and commands for the robot to follow
	 * @return void
	 */
	function translate_cmd($cmd){
		$cmd = strtoupper($cmd);//set the text to all uppercase to not have to worry about case syntax
		$keyword_position = $this->search_keyword($cmd);
		if($keyword_position !== null){
			$cmds = $this->get_commands($keyword_position,$cmd);
			
			//move/direction commands
			for($i=0; $i < sizeof($cmds); $i++){

				if(is_numeric($cmds[$i])){
					//places the robot on the tabletop
					$error = $this->jump($cmds[$i],$cmds[$i+1]);
					$i++;//skip the next array element, assuming it is the second coord
					if($error){
						echo "<strong>{$error}</strong>";
						return;
					}
				}else if(in_array($cmds[$i],self::COMPASS)){//direction detection
					$this->set_direction($cmds[$i],"COMPASS");
				}else if($cmds[$i] == self::MOVE_KEYWORD){
					if($this->direction == 0 || $this->direction == 2){
						$this->travel($this->direction);//if it's north or south
					}else{
						$this->move($this->direction);//if it's west or east
					}
				}elseif(in_array($cmds[$i],self::TURN)){
					$this->set_direction($cmds[$i],"TURN");
				}else if($cmds[$i] === self::REPORT_KEYWORD){
					$this->report();
				}else{
					//unknown command, do nothing
				}
				
			}
		}else{
			echo "<br/><strong>No valid keyword</strong><br/>";
		}
	}

	/**
	 * Step to next/prev array location on current array north and south
	 *
	 * @param [string] $direction - which direction we are going on the y axis
	 * @return void
	 */
	function travel($direction){
		switch(self::COMPASS[$direction]){
			case "NORTH":
				$this->jump($this->x,$this->y+1);
			break;
			case "SOUTH":
				$this->jump($this->x,$this->y-1);
			break;
			
			default:"<br/>you don't belong here<br/>";
		}
	}

	/**
	 * Step to next/prev array location on current array east and west
	 *
	 * @param [string] $direction - which direction we are going on the x axis
	 * @return void
	 */
	function move($direction){	
		switch(self::COMPASS[$direction]){
			case "WEST":
				$this->jump($this->x-1, $this->y);
			break;
			case "EAST":
				$this->jump($this->x+1, $this->y);
			break;
			default:"<br/>what! how did you even get here?<br/>";
		}
	}

	/**
	 * Get the last currenly used direction
	 *
	 * @return void
	 */
	function get_direction(){
		return self::COMPASS[$this->direction];
	}

	/**
	 * Get the direction of the robot whether it's placed or turns around
	 *
	 * @param [string] $pointer - Text version of which direction we are pointing
	 * @param [string] $action - path way action of if we are after the compass direction or if we are turning
	 * @return void
	 */
	function set_direction($pointer,$action){
		if($action == "COMPASS"){
			$direction = array_search($pointer,self::COMPASS);
		}else{
			$direction = $this->turn($pointer);
		}
		
		$this->direction = $direction;
	}

	/**
	 * Handle the turning function to change direction relative to it's compass
	 *
	 * @param [type] $pointer
	 * @return [int] $changed_direction - number relative to the direction
	 */
	function turn($pointer){
		if($pointer == "LEFT"){
			$changed_direction = $this->direction-1;
		}else{
			$changed_direction = $this->direction+1;
		}
		
		if($changed_direction < 0){
			$changed_direction = sizeof(self::COMPASS)-1;
		}else if($changed_direction > sizeof(self::COMPASS)-1) {
			$changed_direction = 0;
		}
		return $changed_direction;

	}

	/**
	 * Rest the robot for the unit testing
	 *
	 * @return void
	 */
	function reset(){
		$this->x = 0;
		$this->y = 0;
		$this->direction = 0;
	}

	/**
	 * Clean up the string of any wild characters or random spaces, carrage returns ect
	 *
	 * @param [string] $command - dirty string
	 * @return [string] $command - clean string
	 */
	function clean_string($command){
		$command = preg_replace('/\s+\n/', ",", $command);
		htmlentities($command);
		$command = str_replace(" ",",",$command);//clean spaces and carrage returns and any other html characters
		return $command;
	}
}
?>