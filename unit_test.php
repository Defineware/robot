<?php
#include "data_object.php";
include "tabletop.php";
include "robot.php";

$tabletop = new tabletop();
$robot = new robot($tabletop);
 
#test cases
echo "<strong>Example a</strong><br/>";
echo "Command: PLACE 0,0,NORTH
MOVE
REPORT<br/>";
$robot->translate_cmd("PLACE 0,0,NORTH,MOVE,REPORT");
$robot->reset();
echo "<br/>Expected Output<br/>
0,1,NORTH
";

echo "<pre>--------------------</pre>";
echo "<strong>Example b</strong><br/>";
echo "Command: PLACE 0,0,NORTH
LEFT
REPORT<br/>";
$robot->translate_cmd("PLACE 0,0,NORTH,LEFT,REPORT");
$robot->reset();
echo "<br/>Expected Output<br/>
0,0,WEST
";

echo "<pre>--------------------</pre>";
echo "<strong>Example c</strong><br/>";
echo "Command: PLACE 1,2,EAST
MOVE
MOVE
LEFT
MOVE
REPORT<br/>";
$robot->translate_cmd("PLACE 1,2,EAST,MOVE,MOVE,LEFT,MOVE,REPORT");
$robot->reset();
echo "<br/>Expected Output<br/>
3,3,NORTH
";
 
echo "<pre>------------------------</pre>";
echo "<pre>-----UNIT TESTING-------</pre>";
echo "<pre>------------------------</pre>";
echo "Test Cases<br/>";
echo "Test with no vaild place keyword<br/>";
echo "Command: 123abcPlace 3,5 SOUTH MOVE MOVE,REPORT";
$robot->translate_cmd("123abcPlace 3,5,SOUTH,MOVE,MOVE,REPORT");
$robot->reset();
echo "<br/>Expected Output<br/>
Error Message
";

echo "<pre>--------------------</pre>";
echo "Test with place outside of table area<br/>";
echo "Command: Place 8,3 SOUTH MOVE MOVE,REPORT";
$robot->translate_cmd("Place 8,3,SOUTH,MOVE,MOVE,REPORT");
$robot->reset();
echo "<br/>Expected Output<br/>
Error Message
";

echo "<pre>--------------------</pre>";
echo "Test with mixed case text<br/>";
echo "Command: PlaCe 2,3,EAsT,MOVe,SoUtH,MoVE,REPoRt";
$robot->translate_cmd("PlaCe 2,3,EAsT,MOVe,SoUtH,MoVE,REPoRt");
$robot->reset();
echo "<br/>Expected Output<br/>
3,2 SOUTH
";

echo "<pre>--------------------</pre>";
echo "Test with place move outside of table area<br/>";
echo "Command: Place 3,3,NORTH,MOVE,MOVE,REPORT";
$robot->translate_cmd("Place 3,3,NORTH,MOVE,MOVE,REPORT");
$robot->reset();
echo "<br/>Expected Output<br/>
3,4,NORTH
";  

echo "<pre>--------------------</pre>";
echo "Test with spaces instead of commas <br/>";
echo "Command: Place 3,3 NORTH MOVE MOVE,REPORT";
$robot->translate_cmd("Place 3,3 WEST MOVE MOVE,REPORT");
$robot->reset();
echo "<br/>Expected Output<br/>
1,3,WEST
";
 
echo "<pre>--------------------</pre>";
echo "Test with carrage returns <br/>";
echo "Command: Place 1,0 
EAST 
MOVE
SOUTH 
MOVE
REPORT";
$robot->translate_cmd("Place 1,0 
EAST 
MOVE
SOUTH 
MOVE
REPORT");
$robot->reset();
echo "<br/>Expected Output<br/>
2,0,SOUTH
"; 

echo "<pre>--------------------</pre>";
echo "Test with mulitiple reports <br/>";
echo "Command: Place 2,2 REPORT 
SOUTH MOVE
MOVE,REPORT";
$robot->translate_cmd("Place 2,2 REPORT 
SOUTH MOVE
MOVE,REPORT");
$robot->reset();
echo "<br/>Expected Output<br/>
2,2 NORTH<br/>
2,0 SOUTH
";

echo "<pre>--------------------</pre>";
echo "Test with mulitiple reports and place <br/>";
echo "Command: Place 2,2 REPORT PLace
SOUTH MOVE
MOVE,REPORT";
$robot->translate_cmd("Place 2,2 REPORT PLace
SOUTH MOVE
MOVE,REPORT");
$robot->reset();
echo "<br/>Expected Output<br/>
2,2 NORTH<br/>
2,0,SOUTH
";

echo "<pre>--------------------</pre>";
echo "Test with mulitiple reports and place <br/>";
echo "Command: Place a,b REPORT ";
$robot->translate_cmd("Place a,b REPORT PLace
SOUTH MOVE
MOVE,REPORT");
$robot->reset();
echo "<br/>Expected Output<br/>
0,0 NORTH
0,0 SOUTH
";  
?>