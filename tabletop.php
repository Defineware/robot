<?php 
/**
 * This tabletop class controls all functions that is required to build the tabletop for the robot to use
 * Php Version 7.1
 * 
 * @author Steven Field <steven@defineware.com>
 * @version 1.0
 * @datetime 7/10/20
 */
class tabletop {
	const X_SIZE = 5;
	const Y_SIZE = 5;
	
	var $table_top = array();

	/**
	 * will create the table on creation
	 */
	function __construct(){
		$this->build_tabletop();
	}
	
	/**
	 * Constructs the tabletop space
	 *
	 * @return void
	 */
	function build_tabletop(){
		for($i =0; $i < self::Y_SIZE; $i++){
			$this->table_top[$i] = array_fill(0,self::X_SIZE,'');
		}
	}
}
?>